
from time import sleep
from flask import Flask
from flask_restful import Api, Resource, reqparse
import threading
import sqlite3
import json
import serial
import sys
ser = serial.Serial()
app = Flask(__name__)
api = Api(app)
db = '/home/shared/LCCOperation/LCCOperation/potColors.db'
inStates = []
outStates = [0,0,0,0,0,0,0,0]
position = 13
feedback = 7

pinchValve = 3
prePurgeTime = 1
cylinder = 4
mult = 27500
muliplier = 4
purge = 12.5
offset = 3
prgVlv = 2 #Need to verify this pin number
prgPulseTime = 2
prgPulseCycls = 5

try:
    ser = serial.Serial( port='/dev/ttyACM0', baudrate=115200, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS, timeout=1)
    print("Connected To Scanner")
    ser.close()
except:
    print("Unable to connect to serial device")
    sys.exit()

def readInputs():
    global inStates
    ser.open()
    ser.write(str("i").encode('ascii'))
    i = 0
    while (ser.in_waiting < 1 and i < 10):
        sleep(.1)
        i = i + 1
        continue    
    try:
        inputs = int(ser.read())
        inStates = [1 if inputs & (1 << (7-n)) else 0 for n in range(8)]
    except:
        inStates = [0,0,0,0,0,0,0,0]
    ser.close()
    return
def homeServo():
    ser.open()
    ser.write(str("h").encode('ascii'))
    ser.close()
    while True:
        readInputs()
        if inStates[feedback]:
            break
        sleep(.1)
    return    

def writeOutputs():    
    byte = 0
    for x in range(0, 8):
        try:
            bit = outStates[x]
        except StopIteration:
            bit = 0
        byte = (byte << 1) | bit
    ser.open()
    ser.write(str("o").encode('ascii'))
    ser.write(bytes([byte]))
    ser.close()
    #ser.write(bytes([255]))
    return byte

def writePosition(dirString, position):
    ser.open()
    ser.write(dirString.encode('ascii'))
    ser.write(bytes([position]))
    ser.close()
    return




currentGUID = 0
readyForPurge = False
readyToSpray = False
currentColor = 'NONE'
maxStations = 30
newRecipe = 1


def getCurrentStation():
    global newRecipe
    conn = sqlite3.connect(db)
    conn.row_factory = sqlite3.Row
    c = conn.execute('SELECT * FROM stations WHERE isActive=1')
    row = c.fetchone()
    newRecipe = row['recipe']
    return row['station']
    
def getStation(color):
    conn = sqlite3.connect(db)
    conn.row_factory = sqlite3.Row
    coltup = (color,) #Creates a tuple of values to prevent sql injection
    c = conn.execute('SELECT * FROM stations WHERE color=?', coltup)
    try:
        row = c.fetchone()
        station = row['station']
        recipe = row['recipe']
    except:
        station = 0
        recipe = 1
    conn.close()
    return station, recipe
def startColorChange(station, color, recipe):
    global currentColor
    global readyToSpray 
    global currentStation
    global newRecipe
    goPurge()
    changeStation(station,purge)    
    conn = sqlite3.connect(db)
    conn.row_factory = sqlite3.Row
    conn.execute('''UPDATE stations SET isActive = 0 WHERE station = ?''',(currentStation,))
    conn.execute('''UPDATE stations SET isActive = 1 WHERE station = ?''',(station,))
    conn.commit()
    currentColor = color
    currentStation = station
    newRecipe = recipe
    readyToSpray = True
    return
    
def changeStation(station, current=getCurrentStation()):
    global outStates
    global inStates    
    dirString = ""
    moveDist = station - current
    if moveDist < 0:
        dirString = "pr"
    elif moveDist > 0:
        dirString = "pf"
    outStates[pinchValve] = 1
    writeOutputs()
    sleep(.5)
    outStates[cylinder] = 0 #Lift Cylinder
    writeOutputs()
    sleep(.75) #Waiting for cylinder to lift
    writePosition(dirString, int(2*abs(moveDist)))
    sleep(.5)
    while True:
        readInputs()
        if inStates[feedback]:
            break
        sleep(.1)
    sleep(.5)
    outStates[pinchValve] = 0
    outStates[cylinder] = 1 #Lower Cylinder
    writeOutputs()
    sleep(.5)
    return

def goPurge():
    global readyForPurge
    
    changeStation(purge)
    while True:
        if readyForPurge: #Wait for gun to bottom
            for _ in range(prgPulseCycls): #double check this syntax
                outStates[prgVlv] = 1
                writeOutputs()
                sleep(prgPulseTime)
                outStates[prgVlv] = 0
                writeOutputs()
                sleep(.5)
            readyForPurge = False
            break
        else:
            sleep(1)
    return

class ColorList(Resource):
    def get(self):
        #get entire list
        conn = sqlite3.connect(db)
        conn.row_factory = sqlite3.Row
        data = conn.execute('''SELECT * FROM stations ORDER BY station ASC''').fetchall()
        conn.close()     
        return [dict(ix) for ix in data]

    def put(self):
        #update paint in a station
        global maxStations
        parser = reqparse.RequestParser()
        parser.add_argument('station', type=int, help='Station for which the color is being updated')
        parser.add_argument('color', type=str, help='P-code for the new color in the station') #Used to avoid repeating color change
        parser.add_argument('colorName', type=str, help='Common name for the color')
        parser.add_argument('recipe', type=int, help='Program number for the gun controller')
        args = parser.parse_args()
        stat = args['station']
        col = args['color']
        colName = args['colorName']
        recipe = args['recipe']
        if stat > maxStations:
            return 'Invalid Station Number', 400
        conn = sqlite3.connect(db)
        conn.row_factory = sqlite3.Row
        conn.execute('''UPDATE stations SET color = ?, colorName = ?, recipe = ? WHERE station = ?''',(col, colName, recipe, stat,))
        conn.commit()
        if conn.total_changes == 0:
            conn.execute('''INSERT into stations VALUES (?, ?, ?, ?)''', (stat, col, colName, recipe,))
            conn.commit()
        data = conn.execute('''SELECT * FROM stations WHERE station=?''', (stat,)).fetchall()    
        conn.close()
        return [dict(ix) for ix in data]

class ColorListItem(Resource):
    def get(self, station):
        #get paint in a particular station
        conn = sqlite3.connect(db)
        conn.row_factory = sqlite3.Row
        stat = (station,) #Creates a tuple of values to prevent sql injection
        data = conn.execute('SELECT * FROM stations WHERE station=? ', stat).fetchone()
        conn.close()
        return [dict(ix) for ix in data]

class ChangeColor(Resource):
    def post(self):
        global readyToSpray
        global currentGUID
        parser = reqparse.RequestParser()
        parser.add_argument('color', type=str, help='P-code for next paint color')
        parser.add_argument('GUID', type=int, help='ID associated with this color change') #Used to avoid repeating color change
        args = parser.parse_args()
        if currentGUID != args['GUID']:                
            #Add this to a log somewhere...
            #'potColors.db' (table: stations; station(int): color(string))
            col = args['color']
            print(col)
            #getStation(col)
            station, recipe = getStation(col)
            if station == 0:
                return 'Requested Color Is Not Loaded In System', 400
            startChange = threading.Thread(target=startColorChange, args=(station,col,recipe,),) #Need getStation(color, start(overload))
            startChange.start()
            currentGUID = args['GUID']
            readyToSpray = False
            return 'ColorChangeStarted', 202 
        else:
            return 'ThisColorChangeAlreadyStarted', 204
class ReadyForPurge(Resource):
    def post(self):
        global readyForPurge
        readyForPurge = True
        return 'PurgingStarted', 202

class ReadyToSpray(Resource):
    def get(self):
        global readyToSpray
        global currentColor
        global newRecipe
        if readyToSpray:
            return {'readyToSpray': True, 'color': currentColor, 'recipe': newRecipe}
        else:
            return {'readyToSpray': False, 'color': 'NONE', 'recipe': newRecipe}, 202


api.add_resource(ChangeColor, '/changeColor')
api.add_resource(ReadyForPurge, '/readyForPurge')
api.add_resource(ReadyToSpray, '/readyToSpray')
api.add_resource(ColorList, '/colorList')
api.add_resource(ColorListItem, '/colorList/<station>')




sleep(3)
outStates[cylinder] = 0
outStates[pinchValve] = 1
writeOutputs()
sleep(1)
homeServo()
sleep(1)
currentStation = getCurrentStation()
changeStation(currentStation, 1)
readyToSpray = True    
if __name__ =='__main__':

    app.run(host='0.0.0.0', debug=True, port=5000)
